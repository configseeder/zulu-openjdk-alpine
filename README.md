# Zulu OpenJDK Alpine

Goal of this repository is to have weekly updated `azul/zulu-openjdk-alpine:21` images.

It can be used by pulling from `registry.gitlab.com/configseeder/zulu-openjdk-alpine:21`

